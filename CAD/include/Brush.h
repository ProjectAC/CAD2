#pragma once

#include <vector>
#include "Vec2.h"
#include "definitions.h"

namespace ACCAD
{
    struct Pen
    {
        Pen(const Color& color, float width) : width(width), color(color) {}
        float width;
        Color color;
    };
}