#pragma once

#include <exception>

namespace ACCAD
{
    class EmptyStackException : public std::exception {};
}