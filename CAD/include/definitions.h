#pragma once

#include <iostream>

#define QT 

namespace ACCAD
{
    typedef unsigned int uint;

    struct Color
    {
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char a;
        Color(unsigned char r = 0, unsigned char g = 0, unsigned char b = 0, unsigned char a = 0);
    };
    std::istream& operator>>(std::istream& in, Color& color);
    std::ostream& operator<<(std::ostream& out, const Color& color);

	const int delta[8][2] = {
        { 1,  0 },
        { 1,  1 },
        { 0,  1 },
        { -1,  1 },
        { -1,  0 },
        { -1, -1 },
        { 0, -1 },
        { 1, -1 }
    };

    const float PI = 3.1415926535f;
    const float radiusBorder = 5.5f;
    const float radiusAnchor = 5.5f;
    const Color borderBorderColor = { 0, 0, 0, 255 };
    const Color borderInnerColor = { 0, 255, 0, 127 };
    const Color anchorBorderColor = { 0, 0, 0, 255 };
    const Color anchorInnerColor = { 255, 0, 0, 255 };
    const Color borderEdgeColor = { 160, 160, 160, 160 };
}
