#pragma once

#include <vector>
#include "Exception.h"
#include "definitions.h"

namespace ACCAD
{
    template<class T>
    class Stack : protected std::vector<T>
    {
    public:

        Stack()
        {
            sp = 0;
        }

        bool upToDate()
        {
            return sp == size();
        }

        void push(const T &val)
        {
            rollback();
            push_back(val);
        }

        const T &top()
        {
            if (sp == 0)
                throw EmptyStackException();
            return this->operator[](sp - 1);
        }

        void pop()
        {
            if (sp == 0)
                throw EmptyStackException();
            sp--;
        }

        void rollback()
        {
            while (!upToDate()) pop_back();
        }

    private:

        uint sp;
    };
}