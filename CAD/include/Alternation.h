#pragma once

#include "definitions.h"
#include "Operation.h" 
#include "Figure.h"
#include "Image.h"

namespace ACCAD
{
    class Alternation : public IOperation
    {
    public:

        /* Alter this Figure
         */
        void redo(Image &image) override;

        /* Cancel this alteration
         */
        void undo(Image &image) override;

        /* [Constructor]
         */
        Alternation(IFigure * origin, int index);

        void AddTarget(IFigure * Target);

        ~Alternation();

    private:
        IFigure* target;
        IFigure* origin;
        int index;
    };
}
