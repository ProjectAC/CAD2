#pragma once

#include <vector>
#include "definitions.h"
#include "Vec2.h"
#include <GL/freeglut.h>

namespace ACCAD
{
    class Renderer
    {
    public:

#if defined MFC

#include <Windows.h>
        
        /* Initialize the renderer
         * Maybe it'll need a Handle of a window?
         */
        void init(HWND hWnd);

        /* Flush the buffer
         * Then swap buffers
         */
        void flush();

#elif defined QT

        /* Initialize the renderer
         * Maybe it'll need a Handle of a window?
         */
        void init(uint width, uint height);

#endif

        /* Begin drawing a frame
        */
        void begin();

        /* End drawing a frame
        */
        void end();

        /* Push matrix to given Position & Rotation
         */
        void push(const Vec2 &center, float theta);

        /* Pop matrix
         */
        void pop();

        static uint getTexureId();

        /* Generate Background Image
         */
        static void generateTexture(Color *c, uint w, uint h, uint &texId);

        /* Render Background image
         */
        void render(uint texId, int l, int r, int d, int u);

        /* Render Polygonal line
         */
        void render(const Color &cborder, const std::vector<Vec2> vertices);

        /* Render a figure
         */
        void render(const Color &cborder, const Color &cinner, const std::vector<Vec2> &vertices, bool show);

        /* Render a figure
         */
        void border(float left, float right, float bottom, float top);

        /* Render the background
         */

        Renderer();
        ~Renderer();

    private:

#if defined MFC
        
        HWND hWnd;
        HDC hDC;
        HGLRC hRC;

#elif defined QT


#endif
        GLuint *index;
        GLfloat *verts;
    };
}