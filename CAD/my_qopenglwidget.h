#pragma once

#include <QOpenGLWidget>
#include "include/Editor.h"

class my_QOpenGLWidget : public QOpenGLWidget
{
	Q_OBJECT

public:

	my_QOpenGLWidget(QWidget *parent);
	~my_QOpenGLWidget();

	void initializeGL() override;
	void resizeGL(int width, int height) override;
	void paintGL() override;

    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

    void setForegroundColor(const QColor& color);
    void setBackgroundColor(const QColor& color);
    QColor getForegroundColor();
    QColor getBackgroundColor();

    void SetPenWidth(unsigned int width);
private:
    
    ACCAD::Editor editor;
    uint w, h;
private slots:
	void create_rectangle_slot();
	void create_circle_slot();	
};

