#include "stdafx.h"

#include "../include/Figure.h"

using namespace std;

namespace ACCAD
{
    FigureType IFigure::getType()
    {
        return UNKNOWN;
    }

    float IFigure::getTheta()
    {
        return theta;
    }

    void IFigure::rotate(int id, const Vec2 &to)
    {
        Vec2 newt = to - center;
        //theta = atan2(newt.y, newt.x);
        Vec2 oldt = (getBorder(id) - center).rotate(0, -theta);
        theta = atan2(newt.y, newt.x) - atan2(oldt.y, oldt.x);
    }

    void IFigure::move(const Vec2 & from, const Vec2 & to)
    {
        center = center + (to - from);
    }

    void IFigure::fillColor(const Color & couter, const Color & cinner)
    {
        this->borderColor = couter;
        this->innerColor = cinner;
    }

    IFigure::IFigure(const Vec2 & center, float theta, const Color &cborder, const Color &cinner) :
        center(center),
        theta(theta),
        borderColor(cborder),
        innerColor(cinner),
        updated(true)
    {

    }

    IFigure::IFigure()
    {
    }
}