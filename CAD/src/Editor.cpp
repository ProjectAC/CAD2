#include "../include/Editor.h"
#include "../include/Vec2.h"
#include <algorithm>

using namespace std;

namespace ACCAD
{
    Editor::Editor() :
        image(),
        strokeManager(image),
        alterManager(image),
        mode(IDLE),
        selectedIndex(-1)
    {
    }

    bool Editor::isFigureSelected()
    {
        return selectedIndex != -1;
    }

    void Editor::startDraw(const Vec2i& from)
    {
        strokeManager.setPen(foreground, penWidth);
        strokeManager.startDraw(from);
    }

    void Editor::finishDraw()
    {
        Stroke* stroke = strokeManager.finishDraw();
        if (stroke != nullptr)
        {
            stack.push(stroke);
        }
    }

    void Editor::movePen(const Vec2i & to)
    {
        strokeManager.movePen(to);
        //update();
    }

    void Editor::setPenWidth(unsigned int width)
    {
        strokeManager.setPen(foreground, width);
    }

    void Editor::insertFigure(PersetFigure figureType, const Vec2i & center, float a, float b)
    {
        //IFigure * figure;
        switch (figureType)
        {
        case Editor::PersetFigure::Ellipse:
        {
            IFigure& figure = ACCAD::Ellipse(center, 0.0f, foreground, background, a, b);
            selectedIndex = image.insertFigure(&figure);
            alterManager.startAlter(selectedIndex, AlterManager::Resize, 1, Vec2(center) + Vec2(a, b), true);
            break; 
        }
        case Editor::PersetFigure::Rectangle:
        {
            IFigure & figure = Polygon(foreground, background, { Vec2(center) + Vec2(a,b), Vec2(center) + Vec2(-a,b), Vec2(center) + Vec2(-a,-b), Vec2(center) + Vec2(a,-b) });
            selectedIndex = image.insertFigure(&figure);
            alterManager.startAlter(selectedIndex, AlterManager::Resize, 1, Vec2(center) + Vec2(a, b), true);
            break;
        }
        case Editor::PersetFigure::Line:
        {
            IFigure & figure = Polygon(foreground, background, { Vec2(center), Vec2(center) + Vec2(a,b) });
            selectedIndex = image.insertFigure(&figure);
            alterManager.startAlter(selectedIndex, AlterManager::Vertex, 1, Vec2(center) + Vec2(a, b), true);
            break;
        }
        default:
            throw exception();
        }
    }

    void Editor::finishInsert()
    {
        Alternation* tmpAlter = alterManager.finishAlter();
        delete tmpAlter;
        Insertion* insert = new Insertion(image.getFigure(selectedIndex));
        stack.push(insert);
    }

    void Editor::startPolygon()
    {
        
    }

    void Editor::finishPolygon()
    {
        Polygon figure(foreground, background, insertBuffer);
        selectedIndex = image.insertFigure(&figure);
        alterManager.startAlter(selectedIndex, AlterManager::Vertex, 0, insertBuffer.front(), true);
        insertBuffer.clear();
    }

    void Editor::addVertex(const Vec2i & vertex)
    {
        if (!insertBuffer.empty() && (Vec2(vertex) - insertBuffer[0]).length() < AnchorThreshold)
        {
            finishPolygon();
        }
        else
        {
            insertBuffer.push_back(vertex);
        }
        /*renderer.begin();
        renderer.render(foreground, insertBuffer);
        renderer.end();*/
    }

    void Editor::startAlter(MouseKeys mouse, const Vec2i& from)
    {
        
        AlterManager::AlterMode alterMode;
        switch (mouse)
        {
        case Editor::Left:
            alterMode = AlterManager::Move;
            break;
        case Editor::Right:
            alterMode = AlterManager::Vertex;
            break;
        default:
            alterMode = AlterManager::Move;
            break;
        }
        alterManager.startAlter(selectedIndex, alterMode, -1, from);
    }

    bool Editor::finishAlter()
    {
        Alternation* alter = alterManager.finishAlter();
        if (alter != nullptr)
        {
            stack.push(alter);
        }
        return alter != nullptr;
    }

    void Editor::alterFigure(const Vec2i & to)
    {
        alterManager.alterFigure(to);
    }

    bool Editor::setAlterMode(const Vec2i & point, MouseKeys mouse)
    {
        //操作方式：
        //左键：
        //  8个锚点为圆心的8个圆：缩放
        //  4个角外部的扇形区域：旋转
        //  图形中心为圆心的圆：移动
        //右键：
        //  多边形指定顶点移动
        IFigure * figure = image.getFigure(selectedIndex);
        AlterManager::AlterMode alterMode = AlterManager::None;
        Vec2 from;

        int anchorID = -1;
        switch (mouse)
        {
        case MouseKeys::Left:
        {
            auto anchors = figure->getBorder();
            for (int i = 0; i < anchors.size(); i++)
            {
                if ((anchors[i] - point).length() < AnchorThreshold)
                {
                    alterMode = AlterManager::Resize;
                    from = anchors[i];
                    anchorID = i;
                    goto OutsideSwitch;
                }
            }

            auto center = (anchors[0] + anchors[4]) * 0.5f;
            float a = abs((anchors[0] - center).x);
            float b = abs((anchors[2] - center).y);
            auto r = (Vec2(point) - center).rotate(0, -figure->getTheta());
            for (int i = 1; i < anchors.size(); i += 2)
            {
                if ((anchors[i] - point).length() < RotateThreshold &&
                    abs(r.x) > a &&
                    abs(r.y) > b)
                {
                    alterMode = AlterManager::Rotate;
                    from = anchors[i];
                    anchorID = i;
                    goto OutsideSwitch;
                }
            }

            if (figure->isInside(point))
            {
                alterMode = AlterManager::Move;
                from = point;
                goto OutsideSwitch;
            }
            anchorID = -1;
            alterManager.setAlterMode(AlterManager::None, -1, {});
            break;
        }
        case MouseKeys::Right:
        {
            if (figure->getType() == FigureType::POLYGON)
            {
                auto anchors = static_cast<Polygon*>(figure)->getAnchors();
                for (int i = 0; i < anchors.size(); i++)
                {
                    if ((anchors[i] - point).length() < AnchorThreshold)
                    {
                        alterMode = AlterManager::Vertex;
                        from = anchors[i];
                        anchorID = i;
                        goto OutsideSwitch;
                    }
                }
            }
            break;
        }
        default:
            break;
        }
    OutsideSwitch:
        /*if (anchorID != -1)
        {
            alterManager.setAlterMode(alterMode, anchorID, from);
            return true;
        }
        else
            return false;*/
        alterManager.setAlterMode(alterMode, anchorID, from);
        return alterMode != AlterManager::AlterMode::None;
    }

    void Editor::fillColor()
    {
        alterManager.fillColor(foreground, background);
    }

    void Editor::startLine(const Vec2i & from)
    {
        startPolygon();
        addVertex(from);
        auto& to = from;
        insertBuffer.push_back(to);
    }

    void Editor::drawLine(const Vec2i & to)
    {
        insertBuffer.pop_back();
        insertBuffer.push_back(to);
    }

    void Editor::finishLine()
    {
        finishPolygon();
    }

    void Editor::eraseFigure()
    {
        if (selectedIndex != -1)
        {
            Erasion * erasion = new Erasion(image.getFigure(selectedIndex), selectedIndex);
            image.eraseFigure(selectedIndex);
            stack.push(erasion);
        }
    }

    int Editor::SelectFigure(const Vec2i & point)
    {
        for (int i = image.getFigureCount() - 1; i >= 0; i--)
        {
            if (image.getFigure(i)->isInside(point))
            {
                selectedIndex = i;
                return i;
            }
        }
        return -1;
    }

    Color Editor::getForegroundColor()
    {
        return foreground;
    }

    Color Editor::getBackgroundColor()
    {
        return background;
    }

    void Editor::setForegroundColor(const Color & color)
    {
        foreground = color;
    }

    void Editor::setBackgroundColor(const Color & color)
    {
        background = color;
    }

    void Editor::initImage(uint width, uint height)
    {
        if (dirty) save();
        renderer.init(width, height);
        image.init(width, height);
        fileName = "";
    }

    void Editor::save()
    {
        if (fileName == "")
        {
            // TODO: get fileName
        }
        saveAs(fileName);
    }

    void Editor::saveAs(const std::string &fileName)
    {
        // TODO
    }
    
    void Editor::load(const std::string &fileName)
    {
        this->fileName = fileName;
    }

    void Editor::update()
    {
        renderer.begin();

        // Draw Image
        image.render(renderer, selectedIndex);

        renderer.end();
    }

    void Editor::setMode(Mode mode)
    {
        if (mode == Mode::ALTER)mode = Mode::IDLE;
        this->mode = mode;
    }

    void Editor::mouseDown(MouseKeys mouse, const Vec2i &pos)
    {
        isPressing = true;
        switch (mode)
        {
        case ACCAD::Editor::IDLE:
        {
            if (mouse == MouseKeys::Left)
            {
                selectedIndex = SelectFigure(pos);
                if (selectedIndex != -1)
                {
                    startAlter(mouse, pos);
                    mode = Mode::ALTER;
                }         
            }
            break;
        }
        case ACCAD::Editor::ALTER:
        {
            if (!(startAlter(mouse, pos), setAlterMode(pos, mouse)))
            {
                selectedIndex = SelectFigure(pos);
                if (selectedIndex != -1)  // Select
                {
                    startAlter(mouse, pos);
                }
                else
                {
                    mode = Mode::IDLE;
                }
            }
            break;
        }
        case ACCAD::Editor::INSERT_POLYGON:
        {
            if(insertBuffer.empty())startPolygon();
            addVertex(pos);
            if (insertBuffer.empty())mode = Mode::ALTER;
            break;
        }
        case ACCAD::Editor::INSERT_ELLIPSE:
        {
            insertFigure(PersetFigure::Ellipse, pos);
            break;
        }
        case ACCAD::Editor::INSERT_RECTANGLE:
        {
            insertFigure(PersetFigure::Rectangle, pos);
            break;
        }
        case ACCAD::Editor::INSERT_LINE:
        {
            insertFigure(PersetFigure::Line, pos, 0, 0);
            //startLine(pos);
            break;
        }
        case ACCAD::Editor::FILL:
        {
            if (mouse == MouseKeys::Left)
            {
                selectedIndex = SelectFigure(pos);
                if (selectedIndex != -1)
                {
                    startAlter(mouse, pos);
                    fillColor();
                    finishAlter();
                }
            }
            break;
        }
        case ACCAD::Editor::PEN:
        {
            startDraw(pos);
            break;
        }
        default:
            break;
        }
    }

    void Editor::mouseMove(const Vec2i &pos)
    {
        if (isPressing)
        {
            switch (mode)
            {
            case ACCAD::Editor::IDLE:
            {

                break;
            }
            case ACCAD::Editor::ALTER:
            {
                alterFigure(pos);
                break;
            }
            case ACCAD::Editor::INSERT_POLYGON:
            {
                
                break;
            }
            case ACCAD::Editor::INSERT_ELLIPSE:
            {
                alterFigure(pos);
                break;
            }
            case ACCAD::Editor::INSERT_RECTANGLE:
            {
                alterFigure(pos);
                break;
            }
            case ACCAD::Editor::INSERT_LINE:
            {
                alterFigure(pos);
                //drawLine(pos);
                break;
            }
            case ACCAD::Editor::PEN:
            {
                movePen(pos);
                break;
            }
            default:
                break;
            }
        }
    }

    void Editor::mouseUp(MouseKeys mouse, const Vec2i &pos)
    {
        isPressing = false;
        switch (mode)
        {
        case ACCAD::Editor::IDLE:
        {
         
            break;
        }
        case ACCAD::Editor::ALTER:
        {
            if (!finishAlter())
            {
                selectedIndex = SelectFigure(pos);
            }
            break;
        }
        case ACCAD::Editor::INSERT_POLYGON:
        {
            
            break;
        }
        case ACCAD::Editor::INSERT_ELLIPSE:
        {
            finishInsert();
            mode = Mode::ALTER;
            break;
        }
        case ACCAD::Editor::INSERT_RECTANGLE:
        {
            finishInsert();
            mode = Mode::ALTER;
            break;
        }
        case ACCAD::Editor::INSERT_LINE:
        {
            finishInsert();
            mode = Mode::ALTER;
            /*finishLine();
            if (insertBuffer.empty())mode = Mode::ALTER;*/
            break;
        }
        case ACCAD::Editor::PEN:
        {
            finishDraw();
            break;
        }
        default:
            break;
        }
    }
}