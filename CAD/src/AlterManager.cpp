#include "../include/AlterManager.h"

using namespace std;
using namespace ACCAD;

ACCAD::AlterManager::AlterManager(Image & image)
{
    this->image = &image;
}

void ACCAD::AlterManager::startAlter(int index, AlterMode alterMode, int anchorID, const Vec2& from, bool isDirty)
{
    currentAlter = new Alternation(image->getFigure(index), index);
    this->index = index;
    this->alterMode = alterMode;
    this->anchorID = anchorID;
    this->from = from;
    this->isDirty = isDirty;
}

Alternation * ACCAD::AlterManager::finishAlter()
{
    if (!isDirty)
    {
        delete currentAlter;
        return nullptr;
    }
    else
    {
        currentAlter->AddTarget(image->getFigure(index));
        return currentAlter;
    }
}

void ACCAD::AlterManager::setAlterMode(AlterMode alterMode, int anchorID, const Vec2 &from)
{
    this->alterMode = alterMode;
    this->anchorID = anchorID;
    this->from = from;
}

void ACCAD::AlterManager::alterFigure(const Vec2 & to)
{
    IFigure *selectedFigure = image->getFigure(index);
    switch (alterMode)
    {
    case ACCAD::AlterManager::Vertex:
        if (anchorID != -1)
        {
            static_cast<Polygon*>(selectedFigure)->alter(anchorID, static_cast<Polygon*>(selectedFigure)->getAnchor(anchorID) + to - from);
            if ((to - from).sqrLength() != 0) isDirty = true;
            from = to;
        }
        break;
    case ACCAD::AlterManager::Resize:
        if (anchorID != -1)
        {
            selectedFigure->resize(anchorID, selectedFigure->getBorder(anchorID) + to - from);
            if ((to - from).sqrLength() != 0) isDirty = true;
            from = to;
        }
        break;
    case ACCAD::AlterManager::Rotate:
        if (anchorID != -1)
        {
            selectedFigure->rotate(anchorID, selectedFigure->getBorder(anchorID) + to - from);
            if ((to - from).sqrLength() != 0) isDirty = true;
            from = to;
        }
        break;
    case ACCAD::AlterManager::Move:
        selectedFigure->move(from, to);
        if ((to - from).sqrLength() != 0) isDirty = true;
        from = to;
        break;
    default:
        break;
    }
}

std::vector<Vec2> ACCAD::AlterManager::getAnchors()
{
    IFigure *selectedFigure = image->getFigure(index);

    switch (alterMode)
    {
    case AlterMode::Vertex:
        switch (selectedFigure->getType())
        {
        case FigureType::POLYGON:
            return static_cast<Polygon*>(selectedFigure)->getAnchors();
        default:
            return {};
        }
    case AlterMode::Resize:
    case AlterMode::Move:
    case AlterMode::Rotate:
        return selectedFigure->getBorder();
    default:
        return {};
    }
}

AlterManager::AlterMode ACCAD::AlterManager::getAlterMode()
{
    return alterMode;
}

void ACCAD::AlterManager::fillColor(const Color & foreground, const Color & background)
{
    IFigure *selectedFigure = image->getFigure(index);
    isDirty = true;
    selectedFigure->fillColor(foreground, background);
}
