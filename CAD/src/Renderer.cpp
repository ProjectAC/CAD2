#include "stdafx.h"

#include "../include/Renderer.h"

namespace ACCAD
{
    Renderer::Renderer() :
        index(new GLuint[32768]),
        verts(new GLfloat[65536])
    {

    }

    Renderer::~Renderer()
    {
        delete[] index;
        delete[] verts;
    }

#if defined MFC
    void Renderer::init(HWND hWnd)
    {
        PIXELFORMATDESCRIPTOR pfd;
        int iFormat;

        this->hWnd = hWnd;

        /* get the device context (DC) */
        hDC = GetDC(hWnd);

        /* set the pixel format for the DC */
        ZeroMemory(&pfd, sizeof(pfd));
        pfd.nSize = sizeof(pfd);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW |
            PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        pfd.iPixelType = PFD_TYPE_RGBA;
        pfd.cColorBits = 24;
        pfd.cDepthBits = 16;
        pfd.iLayerType = PFD_MAIN_PLANE;
        iFormat = ChoosePixelFormat(hDC, &pfd);
        SetPixelFormat(hDC, iFormat, &pfd);

        /* create and enable the render context (RC) */
        hRC = wglCreateContext(hDC);
        wglMakeCurrent(hDC, hRC);
    }

    void Renderer::flush()
    {
        glFlush();
        SwapBuffers(hDC);
    }

#elif defined QT

    void Renderer::init(uint width, uint height)
    {
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
        glEnable(GL_MULTISAMPLE);
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);

        //glShadeModel(GL_FLAT);//设置阴影平滑模式
        glShadeModel(GL_SMOOTH);

        glEnable(GL_POLYGON_SMOOTH);
        glHint(GL_POLYGON_SMOOTH, GL_NICEST);
        glEnable(GL_POINT_SMOOTH);
        glHint(GL_POINT_SMOOTH, GL_NICEST);
        glEnable(GL_LINE_SMOOTH);
        glHint(GL_LINE_SMOOTH, GL_NICEST);

        glEnableClientState(GL_VERTEX_ARRAY);

        gluOrtho2D(0, width, height, 0);
    }

#endif

    void Renderer::begin()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//清除屏幕和深度缓存
        glPushMatrix();
    }

    void Renderer::end()
    {
        glPopMatrix();
        glFlush();
    }

    void Renderer::push(const Vec2 &center, float theta)
    {
        glPushMatrix();
        glTranslatef(center.x, center.y, 0);
        glRotatef(theta / PI * 180, 0, 0, 1);
    }

    void Renderer::pop()
    {
        glPopMatrix();
    }

    uint Renderer::getTexureId()
    {
        uint texId;
        glGenTextures(1, &texId);
        return texId;
    }

    void Renderer::generateTexture(Color *c, uint w, uint h, uint &texId)
    {
        uint size = (w * h) << 2;
        GLubyte *rgba = new GLubyte[size];

        // 正着的
        /*
        int pos = (w * h * 4) - (4 * w);
        for (int row = 0; row < h; row++)
        {
            for (int col = 0; col < w; col ++)
            {
                rgba[pos++] = c[pos].r;    // red
                rgba[pos++] = c[pos].g;    // green
                rgba[pos++] = c[pos].b;    // blue
                rgba[pos++] = c[pos].a;    // alpha
            }
            pos = (pos - (w * 4) * 2);
        }
        */

        // 反着的（大概应该用这个）

        /*int pos = 0;
        for (int row = 0; row < h; row++)
        {
            for (int col = 0; col < w; col++)
            {
                rgba[pos++] = c[pos].r;    // red
                rgba[pos++] = c[pos].g;    // green
                rgba[pos++] = c[pos].b;    // blue
                rgba[pos++] = c[pos].a;    // alpha
            }
        }*/
        memcpy(rgba, c, size);

        glBindTexture(GL_TEXTURE_2D, texId);
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, w, h, GL_RGBA, GL_UNSIGNED_BYTE, rgba);
        //glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, rgba);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //Free
        delete[] (rgba);
    }

    void Renderer::render(uint texId, int l, int r, int d, int u)
    {
        glPushMatrix();

        glColor4d(1, 1, 1, 1);
        glBindTexture(GL_TEXTURE_2D, texId);
        glBegin(GL_QUADS);
        glTexCoord2d(0, 0); glVertex2d(l, d);
        glTexCoord2d(1, 0); glVertex2d(r, d);
        glTexCoord2d(1, 1); glVertex2d(r, u);
        glTexCoord2d(0, 1); glVertex2d(l, u);
        glEnd();

        glPopMatrix();
    }

    void Renderer::render(const Color &cborder, const std::vector<Vec2> vertices)
    {
        // Iterator (?)
        uint i, j;

        // Generate Vertex Array & Vertex Index List
        i = 0, j = 0;
        for (auto &v : vertices)
        {
            verts[i++] = v.x;
            verts[i++] = v.y;
            index[j++] = j;
        }

        glVertexPointer(
            2,
            GL_FLOAT,
            0,
            verts
        );

        glColor4ub(cborder.r, cborder.g, cborder.b, cborder.a);
        glDrawElements(
            GL_LINE_STRIP,
            vertices.size(),
            GL_UNSIGNED_INT,
            index
        );

        // Generate Vertex Array & Vertex Index List
        i = 0, j = 0;
        for (auto &v : vertices)
        {
            verts[i++] = v.x + radiusAnchor;
            verts[i++] = v.y;
            index[j++] = j;

            verts[i++] = v.x;
            verts[i++] = v.y + radiusAnchor;
            index[j++] = j;

            verts[i++] = v.x - radiusAnchor;
            verts[i++] = v.y;
            index[j++] = j;

            verts[i++] = v.x;
            verts[i++] = v.y - radiusAnchor;
            index[j++] = j;
        }

        glVertexPointer(
            2,
            GL_FLOAT,
            0,
            verts
        );

        glColor4ub(anchorInnerColor.r, anchorInnerColor.g, anchorInnerColor.b, anchorInnerColor.a);
        glDrawElements(
            GL_QUADS,
            vertices.size() * 4,
            GL_UNSIGNED_INT,
            index
        );
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glColor4ub(anchorBorderColor.r, anchorBorderColor.g, anchorBorderColor.b, anchorBorderColor.a);
        glDrawElements(
            GL_QUADS,
            vertices.size() * 4,
            GL_UNSIGNED_INT,
            index
        );
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    void Renderer::render(const Color &cborder, const Color &cinner, const std::vector<Vec2> &vertices, bool show)
    {
        // Iterator (?)
        uint i, j;

        // Generate Vertex Array & Vertex Index List
        i = 0, j = 0;
        for (auto &v : vertices)
        {
            verts[i++] = v.x;
            verts[i++] = v.y;
            index[j++] = j;
        }
        
        glVertexPointer(
            2,
            GL_FLOAT,
            0,
            verts
        );

        if (cinner.a)
        {
            glColor4ub(cinner.r, cinner.g, cinner.b, cinner.a);
            glDrawElements(
                GL_POLYGON,
                vertices.size(),
                GL_UNSIGNED_INT,
                index
            );
        }

        glColor4ub(cborder.r, cborder.g, cborder.b, cborder.a);
        glDrawElements(
            GL_LINE_LOOP,
            vertices.size(),
            GL_UNSIGNED_INT,
            index
        );

        if (show)
        {
            // Generate Vertex Array & Vertex Index List
            i = 0, j = 0;
            for (auto &v : vertices)
            {
                verts[i++] = v.x + radiusAnchor;
                verts[i++] = v.y;
                index[j++] = j;

                verts[i++] = v.x;
                verts[i++] = v.y + radiusAnchor;
                index[j++] = j;

                verts[i++] = v.x - radiusAnchor;
                verts[i++] = v.y;
                index[j++] = j;

                verts[i++] = v.x;
                verts[i++] = v.y - radiusAnchor;
                index[j++] = j;
            }

            glVertexPointer(
                2,
                GL_FLOAT,
                0,
                verts
            );

            glColor4ub(anchorInnerColor.r, anchorInnerColor.g, anchorInnerColor.b, anchorInnerColor.a);
            glDrawElements(
                GL_QUADS,
                vertices.size() * 4,
                GL_UNSIGNED_INT,
                index
            );
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glColor4ub(anchorBorderColor.r, anchorBorderColor.g, anchorBorderColor.b, anchorBorderColor.a);
            glDrawElements(
                GL_QUADS,
                vertices.size() * 4,
                GL_UNSIGNED_INT,
                index
            );
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

    void Renderer::border(float left, float right, float bottom, float top)
    {
        uint i = 0, j = 0;
        for (auto &d : delta)
        {
            float x = d[0] == 1 ? right : (d[0] == -1 ? left : 0);
            float y = d[1] == 1 ? top : (d[1] == -1 ? bottom : 0);

            verts[i++] = x + radiusBorder;
            verts[i++] = y;
            index[j++] = j;

            verts[i++] = x;
            verts[i++] = y + radiusBorder;
            index[j++] = j;

            verts[i++] = x - radiusBorder;
            verts[i++] = y;
            index[j++] = j;

            verts[i++] = x;
            verts[i++] = y - radiusBorder;
            index[j++] = j;
        }

        glVertexPointer(
            2,
            GL_FLOAT,
            0,
            verts
        );

        glColor4ub(borderInnerColor.r, borderInnerColor.g, borderInnerColor.b, borderInnerColor.a);
        glDrawElements(
            GL_QUADS,
            32,
            GL_UNSIGNED_INT,
            index
        );
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glColor4ub(borderBorderColor.r, borderBorderColor.g, borderBorderColor.b, borderBorderColor.a);
        glDrawElements(
            GL_QUADS,
            32,
            GL_UNSIGNED_INT,
            index
        );
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glColor4ub(borderEdgeColor.r, borderEdgeColor.g, borderEdgeColor.b, borderEdgeColor.a);
        glBegin(GL_LINE_LOOP);
        glVertex2f(left, top);
        glVertex2f(left, bottom);
        glVertex2f(right, bottom);
        glVertex2f(right, top);
        glEnd();
    }
}