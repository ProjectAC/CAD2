#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_cad.h"

class CAD : public QMainWindow
{
	Q_OBJECT

public:
	CAD(QWidget *parent = Q_NULLPTR);

private:
	Ui::CADClass ui;

private slots:
	void start_open_slot();//打开文件动作对应的槽函数  
	void start_save_slot();
	void edit_undo_slot();
	void edit_paste_slot();
	void edit_copy_slot();
	void revise_length_slot();
	void revise_color_slot();
	void aboutbox_slot();	 
	void pen_fore_slot();
	void pen_back_slot();

};
