#include "stdafx.h"
#include "cad.h"




using namespace std;
using namespace ACCAD;
using namespace Qt;


CAD::CAD(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);	
	connect(ui.start_open, SIGNAL(triggered()), this, SLOT(start_open_slot()));
	connect(ui.start_save, SIGNAL(triggered()), this, SLOT(start_save_slot()));
	connect(ui.edit_undo, SIGNAL(triggered()), this, SLOT(edit_undo_slot()));
	connect(ui.edit_copy, SIGNAL(triggered()), this, SLOT(edit_copy_slot()));
	connect(ui.edit_paste, SIGNAL(triggered()), this, SLOT(edit_paste _slot()));
	connect(ui.create_circle, SIGNAL(triggered()), ui.openGLWidget, SLOT(create_circle_slot()));
	connect(ui.create_rectangle, SIGNAL(triggered()), ui.openGLWidget, SLOT(create_rectangle_slot()));
	connect(ui.revise_length, SIGNAL(triggered()), this, SLOT(revise_length_slot()));
	connect(ui.revise_color, SIGNAL(triggered()), this, SLOT(revise_color_slot()));
	connect(ui.aboutbox, SIGNAL(triggered()), this, SLOT(aboutbox_slot()));
	connect(ui.pen_backcolor, SIGNAL(triggered()), this, SLOT(pen_back_slot()));
	connect(ui.pen_forecolor, SIGNAL(triggered()), this, SLOT(pen_fore_slot()));
	
	
	
}
void CAD::start_open_slot() {
	QFileDialog *fileDialog = new QFileDialog(this);
	fileDialog->setWindowTitle(tr("��ͼƬ"));
	fileDialog->setDirectory(".");
	fileDialog->setNameFilter(tr("Images(*.png *.jpg *.jpeg *.bmp)"));
	fileDialog->setFileMode(QFileDialog::ExistingFiles);  
	fileDialog->setViewMode(QFileDialog::Detail);	
	QStringList fileNames;  
	if (fileDialog->exec())
	{
		fileNames = fileDialog->selectedFiles();
	}
	for (auto tmp : fileNames)
		qDebug() << tmp << endl;
}
void CAD::pen_fore_slot() {
    QColor color = QColorDialog::getColor(Qt::white, this, "Foreground Color Pick", QColorDialog::ShowAlphaChannel);
	QString str;
	if (color.isValid())
	{
		str.sprintf("R:%d G:%d B:%d,C:%d", color.red(), color.green(), color.blue(), color.alpha());
		QMessageBox::information(0, "Get Selected Color", str, QMessageBox::Ok, QMessageBox::Yes);
        
	}
}
void CAD::pen_back_slot() {
	QColor color = QColorDialog::getColor(Qt::white, this, "Background Color Pick", QColorDialog::ShowAlphaChannel);
	
	QString str;
	if (color.isValid())
	{
		str.sprintf("R:%d G:%d B:%d,C:%d", color.red(), color.green(), color.blue(),color.alpha());
		QMessageBox::information(0, "Get Selected Color", str, QMessageBox::Ok, QMessageBox::Yes);
	}
}
void CAD::start_save_slot(){};
void CAD::edit_undo_slot(){};
void CAD::edit_paste_slot(){};
void CAD::edit_copy_slot(){};
void CAD::revise_length_slot(){};
void CAD::revise_color_slot(){};
void CAD::aboutbox_slot(){};