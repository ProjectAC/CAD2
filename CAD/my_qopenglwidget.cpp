#include "stdafx.h"
#include "my_qopenglwidget.h"
#include "CAD.h"
#include "include/Ellipse.h"
#include "include/Polygon.h"
#include <GL/freeglut.h>
#include <thread>
#include <vector>
#include <qevent.h>
#include <qnamespace.h>
#include "ui_cad.h"

using namespace std;
using namespace ACCAD;
using namespace Qt;

ACCAD::IFigure *t;
ACCAD::IFigure *s;

void my_QOpenGLWidget::mousePressEvent(QMouseEvent *e)
{
    QPointF tmp = e->localPos();
    MouseButton button = e->button();
    Editor::MouseKeys key = (button & RightButton) ? Editor::MouseKeys::Right : Editor::MouseKeys::Left;
    editor.mouseDown(key, { (int)tmp.x(), (int)tmp.y() });
}

void my_QOpenGLWidget::mouseMoveEvent(QMouseEvent *e)
{
    QPointF tmp = e->localPos();
    editor.mouseMove({ (int)tmp.x(), (int)tmp.y() });
}

void my_QOpenGLWidget::mouseReleaseEvent(QMouseEvent *e)
{
    QPointF tmp = e->localPos();
    MouseButton button = e->button();
    Editor::MouseKeys key = (button & RightButton) ? Editor::MouseKeys::Right : Editor::MouseKeys::Left;
    editor.mouseUp(key, { (int)tmp.x(), (int)tmp.y() });
}

void my_QOpenGLWidget::keyPressEvent(QKeyEvent *)
{
}

void my_QOpenGLWidget::setForegroundColor(const QColor & color)
{
    editor.setForegroundColor({ (unsigned char)color.red(), (unsigned char)color.green(), (unsigned char)color.blue(), (unsigned char)color.alpha() });
}

void my_QOpenGLWidget::setBackgroundColor(const QColor & color)
{
    editor.setBackgroundColor({ (unsigned char)color.red(), (unsigned char)color.green(), (unsigned char)color.blue(), (unsigned char)color.alpha() });
}

QColor my_QOpenGLWidget::getForegroundColor()
{
    //TODO
    throw exception("TODO");
}

QColor my_QOpenGLWidget::getBackgroundColor()
{
    //TODO
    throw exception("TODO");
}

void my_QOpenGLWidget::SetPenWidth(unsigned int width)
{
    editor.setPenWidth(width);
}

my_QOpenGLWidget::my_QOpenGLWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    w(640),
    h(480)
{
}

my_QOpenGLWidget::~my_QOpenGLWidget()
{
}

void my_QOpenGLWidget::initializeGL()
{
    setGeometry(20, 20, w, h);    //设置窗口初始位置和大小
    editor.initImage(w, h);
}

void my_QOpenGLWidget::paintGL()
{
    editor.update();
    QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
}
void my_QOpenGLWidget::resizeGL(int width, int height)
{
	//未使用
	Q_UNUSED(width);
	Q_UNUSED(height);
}
void my_QOpenGLWidget::create_rectangle_slot() {
	
	/*editor.insertFigure(ACCAD::FigureType::POLYGON, { 100,100 });
	editor.finishInsert();
	editor.update();
	QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);*/
    editor.setMode(Editor::Mode::INSERT_RECTANGLE);
    editor.setMode(Editor::Mode::INSERT_LINE);
}
void my_QOpenGLWidget::create_circle_slot() {
	/*editor.insertFigure(ACCAD::FigureType::ELLIPSE, { 100,100 });
	editor.finishInsert();
	editor.update();
	QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);*/
    editor.setMode(Editor::Mode::INSERT_ELLIPSE);
    editor.setMode(Editor::Mode::INSERT_POLYGON);
}

