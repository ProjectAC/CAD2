/********************************************************************************
** Form generated from reading UI file 'cad.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAD_H
#define UI_CAD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "my_qopenglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_CADClass
{
public:
    QAction *start_open;
    QAction *start_save;
    QAction *start_quit;
    QAction *edit_undo;
    QAction *edit_copy;
    QAction *edit_paste;
    QAction *create_rectangle;
    QAction *create_circle;
    QAction *aboutbox;
    QAction *help;
    QAction *pen_width;
    QAction *pen_forecolor;
    QAction *pen_backcolor;
    QAction *paint_2;
    QAction *pen_2;
    QAction *revise_length;
    QAction *revise_color;
    QAction *revise_2;
    QAction *create_polygon;
    QWidget *centralWidget;
    my_QOpenGLWidget *openGLWidget;
    QMenuBar *menuBar;
    QMenu *start;
    QMenu *edit;
    QMenu *mode;
    QMenu *paint;
    QMenu *revise;
    QMenu *pen;
    QMenu *about;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *CADClass)
    {
        if (CADClass->objectName().isEmpty())
            CADClass->setObjectName(QStringLiteral("CADClass"));
        CADClass->resize(680, 550);
        start_open = new QAction(CADClass);
        start_open->setObjectName(QStringLiteral("start_open"));
        start_save = new QAction(CADClass);
        start_save->setObjectName(QStringLiteral("start_save"));
        start_quit = new QAction(CADClass);
        start_quit->setObjectName(QStringLiteral("start_quit"));
        edit_undo = new QAction(CADClass);
        edit_undo->setObjectName(QStringLiteral("edit_undo"));
        edit_copy = new QAction(CADClass);
        edit_copy->setObjectName(QStringLiteral("edit_copy"));
        edit_paste = new QAction(CADClass);
        edit_paste->setObjectName(QStringLiteral("edit_paste"));
        create_rectangle = new QAction(CADClass);
        create_rectangle->setObjectName(QStringLiteral("create_rectangle"));
        QIcon icon;
        icon.addFile(QStringLiteral("../../../icon/rectangle.ico"), QSize(), QIcon::Normal, QIcon::Off);
        create_rectangle->setIcon(icon);
        create_circle = new QAction(CADClass);
        create_circle->setObjectName(QStringLiteral("create_circle"));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../../../icon/ellipse.ico"), QSize(), QIcon::Normal, QIcon::Off);
        create_circle->setIcon(icon1);
        aboutbox = new QAction(CADClass);
        aboutbox->setObjectName(QStringLiteral("aboutbox"));
        help = new QAction(CADClass);
        help->setObjectName(QStringLiteral("help"));
        pen_width = new QAction(CADClass);
        pen_width->setObjectName(QStringLiteral("pen_width"));
        QIcon icon2;
        icon2.addFile(QStringLiteral("../../../icon/line_width.ico"), QSize(), QIcon::Normal, QIcon::Off);
        pen_width->setIcon(icon2);
        pen_forecolor = new QAction(CADClass);
        pen_forecolor->setObjectName(QStringLiteral("pen_forecolor"));
        QIcon icon3;
        icon3.addFile(QStringLiteral("../../../icon/Color_palette.ico"), QSize(), QIcon::Normal, QIcon::Off);
        pen_forecolor->setIcon(icon3);
        pen_backcolor = new QAction(CADClass);
        pen_backcolor->setObjectName(QStringLiteral("pen_backcolor"));
        QIcon icon4;
        icon4.addFile(QStringLiteral("../../../icon/color_paint.ico"), QSize(), QIcon::Normal, QIcon::Off);
        pen_backcolor->setIcon(icon4);
        paint_2 = new QAction(CADClass);
        paint_2->setObjectName(QStringLiteral("paint_2"));
        pen_2 = new QAction(CADClass);
        pen_2->setObjectName(QStringLiteral("pen_2"));
        revise_length = new QAction(CADClass);
        revise_length->setObjectName(QStringLiteral("revise_length"));
        QIcon icon5;
        icon5.addFile(QStringLiteral("../../../icon/revise.ico"), QSize(), QIcon::Normal, QIcon::Off);
        revise_length->setIcon(icon5);
        revise_color = new QAction(CADClass);
        revise_color->setObjectName(QStringLiteral("revise_color"));
        QIcon icon6;
        icon6.addFile(QStringLiteral("../../../icon/color_fill_128px_539562_easyicon.net.ico"), QSize(), QIcon::Normal, QIcon::Off);
        revise_color->setIcon(icon6);
        revise_2 = new QAction(CADClass);
        revise_2->setObjectName(QStringLiteral("revise_2"));
        create_polygon = new QAction(CADClass);
        create_polygon->setObjectName(QStringLiteral("create_polygon"));
        QIcon icon7;
        icon7.addFile(QStringLiteral("../../../icon/polygon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        create_polygon->setIcon(icon7);
        centralWidget = new QWidget(CADClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        openGLWidget = new my_QOpenGLWidget(centralWidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
        openGLWidget->setGeometry(QRect(39, 20, 621, 411));
        CADClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CADClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 680, 23));
        start = new QMenu(menuBar);
        start->setObjectName(QStringLiteral("start"));
        edit = new QMenu(menuBar);
        edit->setObjectName(QStringLiteral("edit"));
        mode = new QMenu(menuBar);
        mode->setObjectName(QStringLiteral("mode"));
        paint = new QMenu(mode);
        paint->setObjectName(QStringLiteral("paint"));
        revise = new QMenu(mode);
        revise->setObjectName(QStringLiteral("revise"));
        pen = new QMenu(mode);
        pen->setObjectName(QStringLiteral("pen"));
        about = new QMenu(menuBar);
        about->setObjectName(QStringLiteral("about"));
        CADClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CADClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        CADClass->addToolBar(Qt::LeftToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CADClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CADClass->setStatusBar(statusBar);

        menuBar->addAction(start->menuAction());
        menuBar->addAction(edit->menuAction());
        menuBar->addAction(mode->menuAction());
        menuBar->addAction(about->menuAction());
        start->addAction(start_open);
        start->addAction(start_save);
        start->addAction(start_quit);
        edit->addAction(edit_undo);
        edit->addAction(edit_copy);
        edit->addAction(edit_paste);
        mode->addAction(paint->menuAction());
        mode->addAction(revise->menuAction());
        mode->addAction(pen->menuAction());
        paint->addAction(create_rectangle);
        paint->addAction(create_circle);
        paint->addAction(create_polygon);
        revise->addAction(revise_length);
        revise->addAction(revise_color);
        pen->addAction(pen_width);
        pen->addAction(pen_forecolor);
        pen->addAction(pen_backcolor);
        about->addAction(aboutbox);
        about->addAction(help);
        mainToolBar->addAction(paint_2);
        mainToolBar->addSeparator();
        mainToolBar->addAction(create_rectangle);
        mainToolBar->addAction(create_circle);
        mainToolBar->addAction(create_polygon);
        mainToolBar->addSeparator();
        mainToolBar->addAction(pen_2);
        mainToolBar->addAction(pen_width);
        mainToolBar->addAction(pen_forecolor);
        mainToolBar->addAction(pen_backcolor);
        mainToolBar->addSeparator();
        mainToolBar->addAction(revise_2);
        mainToolBar->addAction(revise_length);
        mainToolBar->addAction(revise_color);

        retranslateUi(CADClass);
        QObject::connect(start_quit, SIGNAL(triggered()), CADClass, SLOT(close()));
        QObject::connect(create_rectangle, SIGNAL(triggered()), CADClass, SLOT(update()));

        QMetaObject::connectSlotsByName(CADClass);
    } // setupUi

    void retranslateUi(QMainWindow *CADClass)
    {
        CADClass->setWindowTitle(QApplication::translate("CADClass", "CAD", Q_NULLPTR));
        start_open->setText(QApplication::translate("CADClass", "\346\211\223\345\274\200", Q_NULLPTR));
        start_save->setText(QApplication::translate("CADClass", "\344\277\235\345\255\230", Q_NULLPTR));
        start_quit->setText(QApplication::translate("CADClass", "\351\200\200\345\207\272", Q_NULLPTR));
        edit_undo->setText(QApplication::translate("CADClass", "\346\222\244\351\224\200", Q_NULLPTR));
        edit_copy->setText(QApplication::translate("CADClass", "\345\244\215\345\210\266", Q_NULLPTR));
        edit_paste->setText(QApplication::translate("CADClass", "\347\262\230\350\264\264", Q_NULLPTR));
        create_rectangle->setText(QApplication::translate("CADClass", "\347\237\251\345\275\242", Q_NULLPTR));
        create_circle->setText(QApplication::translate("CADClass", "\346\244\255\345\234\206", Q_NULLPTR));
        aboutbox->setText(QApplication::translate("CADClass", "\345\205\263\344\272\216", Q_NULLPTR));
        help->setText(QApplication::translate("CADClass", "\345\270\256\345\212\251", Q_NULLPTR));
        pen_width->setText(QApplication::translate("CADClass", "\347\262\227\347\273\206", Q_NULLPTR));
        pen_forecolor->setText(QApplication::translate("CADClass", "\350\276\271\346\241\206\350\211\262", Q_NULLPTR));
        pen_backcolor->setText(QApplication::translate("CADClass", "\350\203\214\346\231\257\350\211\262", Q_NULLPTR));
        paint_2->setText(QApplication::translate("CADClass", "\347\273\230\345\233\276\345\214\272", Q_NULLPTR));
        pen_2->setText(QApplication::translate("CADClass", "\347\224\273\347\254\224\345\214\272", Q_NULLPTR));
        revise_length->setText(QApplication::translate("CADClass", "\344\277\256\346\224\271\345\214\272", Q_NULLPTR));
        revise_color->setText(QApplication::translate("CADClass", "\350\260\203\346\225\264\345\244\247\345\260\217", Q_NULLPTR));
        revise_2->setText(QApplication::translate("CADClass", "\344\277\256\346\224\271\345\214\272", Q_NULLPTR));
        create_polygon->setText(QApplication::translate("CADClass", "\345\244\232\350\276\271\345\275\242", Q_NULLPTR));
        start->setTitle(QApplication::translate("CADClass", "\345\274\200\345\247\213", Q_NULLPTR));
        edit->setTitle(QApplication::translate("CADClass", "\347\274\226\350\276\221", Q_NULLPTR));
        mode->setTitle(QApplication::translate("CADClass", "\346\250\241\345\274\217", Q_NULLPTR));
        paint->setTitle(QApplication::translate("CADClass", "\347\273\230\345\233\276", Q_NULLPTR));
        revise->setTitle(QApplication::translate("CADClass", "\350\260\203\346\225\264", Q_NULLPTR));
        pen->setTitle(QApplication::translate("CADClass", "\347\224\273\347\254\224\351\200\211\346\213\251", Q_NULLPTR));
        about->setTitle(QApplication::translate("CADClass", "\345\205\263\344\272\216", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CADClass: public Ui_CADClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAD_H
//H
