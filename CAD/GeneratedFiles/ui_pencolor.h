/********************************************************************************
** Form generated from reading UI file 'pencolor.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PENCOLOR_H
#define UI_PENCOLOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_PenColor
{
public:
    QPushButton *pushButton;
    QLabel *label_fore;
    QLabel *label_back;
    QLabel *label_fore_R;
    QLabel *label_fore_G;
    QLabel *label_fore_B;
    QLabel *label_fore_C;
    QLabel *label_back_R;
    QLabel *label_back_G;
    QLabel *label_back_B;
    QLabel *label_back_C;
    QLineEdit *lineEdit_fore_R;
    QLineEdit *lineEdit_fore_G;
    QLineEdit *lineEdit_fore_B;
    QLineEdit *lineEdit_fore_C;
    QLineEdit *lineEdit_back_R;
    QLineEdit *lineEdit_back_G;
    QLineEdit *lineEdit_back_B;
    QLineEdit *lineEdit_back_C;

    void setupUi(QDialog *PenColor)
    {
        if (PenColor->objectName().isEmpty())
            PenColor->setObjectName(QStringLiteral("PenColor"));
        PenColor->resize(400, 300);
        pushButton = new QPushButton(PenColor);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(290, 260, 75, 23));
        label_fore = new QLabel(PenColor);
        label_fore->setObjectName(QStringLiteral("label_fore"));
        label_fore->setGeometry(QRect(50, 40, 61, 16));
        label_back = new QLabel(PenColor);
        label_back->setObjectName(QStringLiteral("label_back"));
        label_back->setGeometry(QRect(240, 40, 71, 20));
        label_fore_R = new QLabel(PenColor);
        label_fore_R->setObjectName(QStringLiteral("label_fore_R"));
        label_fore_R->setGeometry(QRect(20, 70, 16, 16));
        label_fore_G = new QLabel(PenColor);
        label_fore_G->setObjectName(QStringLiteral("label_fore_G"));
        label_fore_G->setGeometry(QRect(20, 110, 16, 16));
        label_fore_B = new QLabel(PenColor);
        label_fore_B->setObjectName(QStringLiteral("label_fore_B"));
        label_fore_B->setGeometry(QRect(20, 150, 16, 16));
        label_fore_C = new QLabel(PenColor);
        label_fore_C->setObjectName(QStringLiteral("label_fore_C"));
        label_fore_C->setGeometry(QRect(20, 190, 16, 16));
        label_back_R = new QLabel(PenColor);
        label_back_R->setObjectName(QStringLiteral("label_back_R"));
        label_back_R->setGeometry(QRect(210, 70, 16, 20));
        label_back_G = new QLabel(PenColor);
        label_back_G->setObjectName(QStringLiteral("label_back_G"));
        label_back_G->setGeometry(QRect(210, 110, 16, 16));
        label_back_B = new QLabel(PenColor);
        label_back_B->setObjectName(QStringLiteral("label_back_B"));
        label_back_B->setGeometry(QRect(210, 150, 16, 16));
        label_back_C = new QLabel(PenColor);
        label_back_C->setObjectName(QStringLiteral("label_back_C"));
        label_back_C->setGeometry(QRect(210, 190, 16, 16));
        lineEdit_fore_R = new QLineEdit(PenColor);
        lineEdit_fore_R->setObjectName(QStringLiteral("lineEdit_fore_R"));
        lineEdit_fore_R->setGeometry(QRect(50, 70, 61, 20));
        lineEdit_fore_G = new QLineEdit(PenColor);
        lineEdit_fore_G->setObjectName(QStringLiteral("lineEdit_fore_G"));
        lineEdit_fore_G->setGeometry(QRect(50, 110, 61, 20));
        lineEdit_fore_B = new QLineEdit(PenColor);
        lineEdit_fore_B->setObjectName(QStringLiteral("lineEdit_fore_B"));
        lineEdit_fore_B->setGeometry(QRect(50, 150, 61, 20));
        lineEdit_fore_C = new QLineEdit(PenColor);
        lineEdit_fore_C->setObjectName(QStringLiteral("lineEdit_fore_C"));
        lineEdit_fore_C->setGeometry(QRect(50, 190, 61, 20));
        lineEdit_back_R = new QLineEdit(PenColor);
        lineEdit_back_R->setObjectName(QStringLiteral("lineEdit_back_R"));
        lineEdit_back_R->setGeometry(QRect(240, 70, 61, 20));
        lineEdit_back_G = new QLineEdit(PenColor);
        lineEdit_back_G->setObjectName(QStringLiteral("lineEdit_back_G"));
        lineEdit_back_G->setGeometry(QRect(240, 110, 61, 20));
        lineEdit_back_B = new QLineEdit(PenColor);
        lineEdit_back_B->setObjectName(QStringLiteral("lineEdit_back_B"));
        lineEdit_back_B->setGeometry(QRect(240, 150, 61, 20));
        lineEdit_back_C = new QLineEdit(PenColor);
        lineEdit_back_C->setObjectName(QStringLiteral("lineEdit_back_C"));
        lineEdit_back_C->setGeometry(QRect(240, 190, 61, 20));

        retranslateUi(PenColor);

        QMetaObject::connectSlotsByName(PenColor);
    } // setupUi

    void retranslateUi(QDialog *PenColor)
    {
        PenColor->setWindowTitle(QApplication::translate("PenColor", "PenColor", Q_NULLPTR));
        pushButton->setText(QApplication::translate("PenColor", "OK", Q_NULLPTR));
        label_fore->setText(QApplication::translate("PenColor", "ForeGround", Q_NULLPTR));
        label_back->setText(QApplication::translate("PenColor", "BackGround", Q_NULLPTR));
        label_fore_R->setText(QApplication::translate("PenColor", "R", Q_NULLPTR));
        label_fore_G->setText(QApplication::translate("PenColor", "G", Q_NULLPTR));
        label_fore_B->setText(QApplication::translate("PenColor", "B", Q_NULLPTR));
        label_fore_C->setText(QApplication::translate("PenColor", "C", Q_NULLPTR));
        label_back_R->setText(QApplication::translate("PenColor", "R", Q_NULLPTR));
        label_back_G->setText(QApplication::translate("PenColor", "G", Q_NULLPTR));
        label_back_B->setText(QApplication::translate("PenColor", "B", Q_NULLPTR));
        label_back_C->setText(QApplication::translate("PenColor", "C", Q_NULLPTR));
        lineEdit_fore_R->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_fore_G->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_fore_B->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_fore_C->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_back_R->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_back_G->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_back_B->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
        lineEdit_back_C->setText(QApplication::translate("PenColor", "0", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PenColor: public Ui_PenColor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PENCOLOR_H
